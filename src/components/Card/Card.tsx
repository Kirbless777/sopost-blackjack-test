import React from "react";
import {Card} from "../../core/interfaces/cards.interface";
import './Card.scss';

export interface CardProps {
    card: Card
}

//1273 * 573

export const CardItem: React.FC<CardProps> = (props: CardProps) => {
    const cardWidth: number = 97.95
    const cardHeight: number = 143.25
    const styleCard: { [style: string]: string | number } = {
        backgroundPosition: `-${props.card.spritePosition.x * cardWidth}px -${props.card.spritePosition.y * cardHeight}px`
    }
    return (
        <div className={'Card'}>
            <div className={'Card_container'}>
                {/*ToDo: user sprite sheet library?*/}
                <div className={'Card_image'} style={styleCard}></div>
            </div>
        </div>)
}
