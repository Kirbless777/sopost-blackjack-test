import React, {useState} from 'react';
import './Game.scss';
// TODO - alias imports
import CardService from "../../core/services/card.service";
import {Card} from "../../core/interfaces/cards.interface";
import {CardItem} from "../../components/Card/Card";

const gameService: CardService = new CardService()

const Game: React.FC = () => {
    const [deck, setDeck] = useState<{ [key: string]: Card }>(gameService.createDeck())
    const shuffle = (): void => {
        setDeck(gameService.shuffle(deck))
    }
    return (
        <div className="Game">
            Hello Game World :)
            {/*Loop through cards*/}
            <div className={'Game_table'}>
                {Object.keys(deck).map((key: string) => {
                    return (<CardItem key={deck[key].id} card={deck[key]}/>)
                })}
            </div>
            <button onClick={shuffle}>Shuffle</button>
        </div>
    );
}

export default Game;
