export interface Card {
    id: number
    displayValue: string
    numericalValues: {
        primary: number
        secondary?: number
    }
    suit: 'spades' | 'hearts' | 'diamonds' | 'clubs'
    spritePosition: {
        x: number
        y: number
    }
}
