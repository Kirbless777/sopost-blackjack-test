import {Card} from "../interfaces/cards.interface";

interface SpecialCard {
    card: 'king' | 'queen' | 'jack' | 'ace'
    suit: 'spades' | 'hearts' | 'diamonds' | 'clubs'
    numericalValues: {
        primary: number
        secondary?: number
    }
    spritePosition: {
        x: number
        y: number
    }
}

interface Suit {
    suit: 'spades' | 'hearts' | 'diamonds' | 'clubs'
    spritePosition: number
}

interface Royal {
    card: 'ace' | 'king' | 'jack' | 'queen'
    spritePosition: number
}

export default class CardService {
    readonly maxNumericalValue: number = 9
    readonly specialCards: SpecialCard[] = []
    readonly suits: Suit[] = [
        {
            suit: 'spades',
            spritePosition: 0
        }, {
            suit: 'hearts',
            spritePosition: 1
        }, {
            suit: 'diamonds',
            spritePosition: 2
        }, {
            suit: 'clubs',
            spritePosition: 3
        }]
    private id: number

    constructor() {
        this.id = 0
    }

    /**
     * Create a new deck of cards
     */
    public createDeck(): { [id: number]: Card } {
        this.id = 0
        const deck: Card[] = [...this.generatePeasantCards(), ...this.generateRoyalFamily()]
        const keyObjectDeck: { [id: string]: Card } = {}
        deck.forEach((card: Card) => keyObjectDeck[card.displayValue] = card)
        return keyObjectDeck
    }

    /**
     * Function that takes a deck of cards and shuffles it right up!
     * @param deck
     */
    public shuffle(deck: {[key: string]: Card}): {[key: string]: Card} {
        const array: Card[] = Object.keys(deck).map((key: string) => deck[key])
        const shuffled: {[key: string]: Card} = {}
        for(let i = array.length - 1; i >= 0 ; i--){
            const index: number = Math.floor(Math.random() * i)
            shuffled[array[index].displayValue] = array[index]
            array.splice(index, 1)
        }
        return shuffled
    }

    /**
     * Generate cards 2 - 10
     * @private
     */
    private generatePeasantCards(): Card[] {
        const deck: Card[] = []
        for (let i = 0; i < this.maxNumericalValue; i++) {
            this.suits.forEach((suit: Suit, j: number) => {
                deck.push({
                    id: this.id,
                    displayValue: `${suit.suit}-${i + 2}`,
                    numericalValues: {
                        primary: i + 2 // needs to be 2, as there is no 1 (its ace)
                    },
                    suit: suit.suit,
                    spritePosition: {
                        x: i + 1,
                        y: suit.spritePosition
                    }
                })
                this.id += 1
            })
        }
        return deck
    }

    /**
     * Will generate the Ace, King, Queen and Jack cards as they are all special
     * @private
     */
    private generateRoyalFamily(): Card[] {
        const cards: Royal[] = [{
            card: 'ace',
            spritePosition: 0
        }, {
            card: 'jack',
            spritePosition: 10
        }, {
            card: 'queen',
            spritePosition: 11
        }, {
            card: 'king',
            spritePosition: 12
        }]

        return cards.map((royal: Royal): Card[] => // loop through cards
            this.suits.map((suit: Suit): Card => {
                    this.id += 1
                    return ({ // loop through suits
                        id: this.id - 1,
                        displayValue: `${suit.suit}-${royal.card}`,
                        suit: suit.suit,
                        // if ace, it has a different value
                        numericalValues: royal.card === 'ace' ? {
                            primary: 10
                        } : {
                            primary: 11,
                            secondary: 1
                        },
                        spritePosition: {
                            y: suit.spritePosition,
                            x: royal.spritePosition
                        }
                    } as Card)
                }
            )
        ).flat()
    }

}
